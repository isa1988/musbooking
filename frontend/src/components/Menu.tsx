import React from 'react';
import { Link } from 'react-router-dom';

const Menu: React.FC = () => {
  return (
    <nav>
      <Link to="unit">Оборудование</Link>
    </nav>
  );
};

export default Menu;
