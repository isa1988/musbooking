import React from 'react';
import ReactDOM from 'react-dom/client';
import axios, { AxiosResponse, AxiosError } from 'axios';
import PopupComponent, { ResultCode } from './PopupComponentProps';

export enum RequestType {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
}

export const evenResponse = async (
  url: string,
  requestData: any,
  requestType: RequestType,
  successFun: (data: any) => void,
  isNotShow?: boolean
): Promise<number> => {
  let responseCode: number;
  let message: string | undefined;
  let messageType: ResultCode;
  try {
    messageType = ResultCode.Success;
    const devConfig = require('../config.dev');
    const prodConfig = require('../config.prod');

    const config =
      process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
    url = config.apiUrl + url;
    let response: AxiosResponse<any> | undefined;

    if (requestType === RequestType.GET) {
      response = await axios.get(url);
    } else if (requestType === RequestType.POST) {
      response = await axios.post(url, requestData);
    } else if (requestType === RequestType.PUT) {
      response = await axios.put(url, requestData);
    } else if (requestType === RequestType.DELETE) {
      response = await axios.delete(url);
    }

    if (response) {
      if (response.data.isSuccess) {
        successFun(response.data);
        message = 'Операция прошла успеша';
        messageType = response.data.code;
        responseCode = 200;
      } else {
        successFun(response.data);
        message = response.data.message;
        messageType = response.data.code;
        responseCode = 200;
      }
    } else {
      messageType = ResultCode.LoginError;
      responseCode = 400;
    }
  } catch (error: unknown) {
    const err = error as AxiosError;
    message = err.message || 'Unknown error';
    messageType = ResultCode.LoginError;

    responseCode = 400;
    console.error(error);
  }

  if (!isNotShow) {
    let container = document.getElementById('popupNotification') as HTMLElement;
    const toastContainer = ReactDOM.createRoot(container);
    toastContainer.render(
      <React.StrictMode>
        <PopupComponent
          message={message || ''}
          messageType={messageType || ResultCode.LoginError}
        />
      </React.StrictMode>
    );
  }
  return responseCode;
};

const GeneralFunction = {
  evenResponse,
};

export default GeneralFunction;
