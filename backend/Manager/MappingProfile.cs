﻿using AutoMapper;
using Core.Entity;
using Core.Filters;
using Core.Filters.Equipment;
using Core.Helper;
using Manager.Dto;
using Manager.Filters;
using Manager.Filters.Equipment;
using Manager.Helper;

namespace Manager
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            GeneralMapping();
            EquipmentMapping();
        }

        public void EquipmentMapping()
        {
            CreateMap<Equipment, EquipmentDto>();
            CreateMap<EquipmentDto, Equipment>();

            CreateMap<EquipmentFilterDto, EquipmentFilter>();
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSetting<>))
                .ForMember("PageSetting",
                opt => opt.MapFrom((src, dest, destMember, context) =>
                    {
                        var pageSize = PageInfoListHelperDto.GetPropValue<int>(src, "PageSize");
                        var startPosition = PageInfoListHelperDto.GetPropValue<int>(src, "StartPosition");
                        return new PagingPageSetting
                        {
                            PageSize = pageSize,
                            StartPosition = startPosition
                        };
                    }));

            CreateMap(typeof(PeriodFilterDto<>), typeof(PeriodFilter<>));

            CreateMap(typeof(QueryOptionsDto<,>), typeof(QueryOptionsForDelete<,>))
                .ForMember("SettingSelectForDelete", p => p.MapFrom(n => Primitive.PagingOrderSettings.SettingSelectForDelete.OnlyUnDelete))
                .ForMember("TrackingBehavior", p => p.MapFrom(n => Primitive.TrackingBehavior.AsTracking));
            CreateMap(typeof(QueryOptionsDto<,>), typeof(QueryOptions<,>))
                .ForMember("TrackingBehavior", p => p.MapFrom(n => Primitive.TrackingBehavior.AsTracking));

            CreateMap(typeof(QueryOptionsDto<,>), typeof(QueryOptionsForDeleteCount<>))
                .ForMember("SettingSelectForDelete", p => p.MapFrom(n => Primitive.PagingOrderSettings.SettingSelectForDelete.OnlyUnDelete))
                .ForMember("TrackingBehavior", p => p.MapFrom(n => Primitive.TrackingBehavior.AsTracking));
            CreateMap(typeof(QueryOptionsDto<,>), typeof(QueryOptionsCount<>))
                .ForMember("TrackingBehavior", p => p.MapFrom(n => Primitive.TrackingBehavior.AsTracking));
        }
    }
}
