﻿using Manager.Service;

namespace Manager.Helper
{
    public record ErrorInfoDto
    {
        public string ErrorMessage { get; set; } = string.Empty;
        public ResultCode Code { get; set; }
        public bool IsError { get { return Code != ResultCode.Success; } }
    }
}
