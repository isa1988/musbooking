﻿using FluentValidation.Results;
using Manager.Service;

namespace Manager.Helper
{
    public static class ErrorHelper
    {
        public static ErrorInfoDto GetError(this ValidationResult? validationResult)
        {
            if (validationResult != null && !validationResult.IsValid && validationResult.Errors.Count > 0)
            {
                var errorCode = ResultCode.FieldIsNull;
                if (validationResult.Errors[0].CustomState is ResultCode code)
                {
                    errorCode = code;
                }
                return new ErrorInfoDto { ErrorMessage = validationResult.Errors[0].ErrorMessage, Code = errorCode };
            }
            return new ErrorInfoDto { Code = ResultCode.Success };

        }
    }
}
