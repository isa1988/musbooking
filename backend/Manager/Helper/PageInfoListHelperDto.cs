﻿using AutoMapper;
using Manager.Filters;

namespace Manager.Helper
{
    public static class PageInfoListHelperDto
    {
        public static PageInfoListDto<TDto, TOrder> CreatePageInfoList<TEntity, TDto, TOrder>
            (this IMapper mapper, List<TEntity> value, int totalCount, PagingOrderSettingDto<TOrder>? orderSetting)
            where TEntity : class
            where TDto : class
            where TOrder : Enum
        {
            var dtoList = mapper.Map<List<TDto>>(value);
            var dto = new PageInfoListDto<TDto, TOrder>
            {
                ValueList = dtoList,
                TotalCount = totalCount,
                OrderSetting = orderSetting
            };
            return dto;
        }

        public static T? GetPropValue<T>(object src, string propName)
        {
            return (T?)src.GetType().GetProperty(propName).GetValue(src, null);
        }
    }
}
