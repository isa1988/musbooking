﻿using BuildingBlocks.CQRS;
using Manager.Dto;
using Manager.Filters;
using Manager.Filters.Equipment;
using Primitive.PagingOrderSettings.Sort;

namespace Manager.Service.Equipment.Queries.GetAll
{
    public record GetAllEquipmentQuery(QueryOptionsDto<EquipmentFilterDto, EquipmentFields> Filter)
        : IQuery<EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>>;
}
