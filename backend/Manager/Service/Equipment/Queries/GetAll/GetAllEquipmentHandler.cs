﻿using BuildingBlocks.CQRS;
using AutoMapper;
using Core.Filters;
using Primitive.MyException;
using Manager.Helper;
using Manager.Dto;
using Manager.Filters;
using Primitive.PagingOrderSettings.Sort;
using Core.Contract.Order;

namespace Manager.Service.Equipment.Queries.GetAll
{
    public class GetAllEquipmentHandler
        : IQueryHandler<GetAllEquipmentQuery, EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IMapper mapper;

        public GetAllEquipmentHandler(IEquipmentRepository equipmentRepository, IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.mapper = mapper;
        }
        public async Task<EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>> Handle(GetAllEquipmentQuery query,
            CancellationToken cancellationToken)
        {
            try
            {

                var filter = mapper.Map<QueryOptions<Core.Filters.Equipment.EquipmentFilter, EquipmentFields>>(query.Filter);

                var filterCount = mapper.Map<QueryOptionsCount<Core.Filters.Equipment.EquipmentFilter>>(query.Filter);

                var EquipmentList = await equipmentRepository.GetAllAsync(filter);
                var count = await equipmentRepository.GetAllCountAsync(filterCount);

                var retVal = mapper.CreatePageInfoList<Core.Entity.Equipment, EquipmentDto, EquipmentFields>(EquipmentList, count, query.Filter.PagingOrderSetting);
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
