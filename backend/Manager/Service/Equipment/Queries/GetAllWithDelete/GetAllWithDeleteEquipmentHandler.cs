﻿using BuildingBlocks.CQRS;
using AutoMapper;
using Core.Filters;
using Primitive.MyException;
using Manager.Helper;
using Manager.Filters;
using Manager.Dto;
using Primitive.PagingOrderSettings.Sort;
using Core.Contract.Order;

namespace Manager.Service.Equipment.Queries.GetAllWithDelete
{
    public class GetAllWithDeleteEquipmentHandler
        : IQueryHandler<GetAllWithDeleteEquipmentQuery, EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IMapper mapper;

        public GetAllWithDeleteEquipmentHandler(IEquipmentRepository equipmentRepository, IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.mapper = mapper;
        }
        public async Task<EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>> Handle(GetAllWithDeleteEquipmentQuery query,
            CancellationToken cancellationToken)
        {
            try
            {
                var filter = mapper.Map<QueryOptionsForDelete<Core.Filters.Equipment.EquipmentFilter, EquipmentFields>>(query.Filter);
                filter.SettingSelectForDelete = Primitive.PagingOrderSettings.SettingSelectForDelete.All;

                var filterCount = mapper.Map<QueryOptionsForDeleteCount<Core.Filters.Equipment.EquipmentFilter>>(query.Filter);
                filter.SettingSelectForDelete = Primitive.PagingOrderSettings.SettingSelectForDelete.All;

                var EquipmentList = await equipmentRepository.GetAllAsync(filter);
                var count = await equipmentRepository.GetAllCountAsync(filterCount);

                var retVal = mapper.CreatePageInfoList<Core.Entity.Equipment, EquipmentDto, EquipmentFields>(EquipmentList, count, query.Filter.PagingOrderSetting);
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Success(retVal);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<PageInfoListDto<EquipmentDto, EquipmentFields>>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
