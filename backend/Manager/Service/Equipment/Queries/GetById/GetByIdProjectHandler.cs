﻿using BuildingBlocks.CQRS;
using AutoMapper;
using Core.Filters;
using Primitive.MyException;
using Manager.Dto;
using Core.Contract.Order;

namespace Manager.Service.Equipment.Queries.GetById
{
    public class GetByIdEquipmentHandler
        : IQueryHandler<GetByIdEquipmentQuery, EntityOperationResult<EquipmentDto>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IMapper mapper;

        public GetByIdEquipmentHandler(IEquipmentRepository equipmentRepository, IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.mapper = mapper;
        }
        public async Task<EntityOperationResult<EquipmentDto>> Handle(GetByIdEquipmentQuery query,
            CancellationToken cancellationToken)
        {
            try
            {
                var filter = new QueryOptionsForDeleteOne<Guid>()
                {
                    Filter = query.Id,
                    SettingSelectForDelete = Primitive.PagingOrderSettings.SettingSelectForDelete.OnlyUnDelete,
                };

                var Equipment = await equipmentRepository.GetByIdAsync(filter);
                var EquipmentDto = mapper.Map<EquipmentDto>(Equipment);
                return EntityOperationResult<EquipmentDto>.Success(EquipmentDto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
