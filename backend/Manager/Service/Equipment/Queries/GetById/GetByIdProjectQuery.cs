﻿using BuildingBlocks.CQRS;
using Manager.Dto;

namespace Manager.Service.Equipment.Queries.GetById
{
    public record GetByIdEquipmentQuery(Guid Id)
        : IQuery<EntityOperationResult<EquipmentDto>>;
}
