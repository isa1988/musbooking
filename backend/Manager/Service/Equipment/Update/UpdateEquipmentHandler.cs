﻿using AutoMapper;
using BuildingBlocks.CQRS;
using Core.Contract.Order;
using Core.Filters;
using FluentValidation;
using Manager.Dto;
using Manager.Helper;
using Primitive.MyException;

namespace Manager.Service.Equipment.Update
{
    public class UpdateEquipmentHandler : ICommandHandler<UpdateEquipmentCommand, EntityOperationResult<EquipmentDto>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IValidator<UpdateEquipmentCommand> validator;
        private readonly IMapper mapper;

        public UpdateEquipmentHandler(IEquipmentRepository equipmentRepository,
            IValidator<UpdateEquipmentCommand> validator,
            IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.validator = validator;
            this.mapper = mapper;
        }

        public async Task<EntityOperationResult<EquipmentDto>> Handle(UpdateEquipmentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var validationResult = await validator.ValidateAsync(command, cancellationToken);
                var error = validationResult.GetError();
                if (error.IsError)
                {
                    return EntityOperationResult<EquipmentDto>.Failure(error.ErrorMessage, error.Code);
                }
                var filter = new QueryOptionsForDeleteOne<Guid>
                {
                    Filter = command.Dto.Id,
                    SettingSelectForDelete = Primitive.PagingOrderSettings.SettingSelectForDelete.All
                };

                var equipment = await equipmentRepository.GetByIdAsync(filter);
                equipment.Name = command.Dto.Name;
                equipment.Price = command.Dto.Price;
                equipment.Amount = command.Dto.Amount;
                equipmentRepository.Update(equipment);

                await equipmentRepository.SaveAsync(cancellationToken);

                var dto = mapper.Map<EquipmentDto>(equipment);

                return EntityOperationResult<EquipmentDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
