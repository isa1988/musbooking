﻿using BuildingBlocks.CQRS;
using Manager.Dto;

namespace Manager.Service.Equipment.Update
{
    public record UpdateEquipmentCommand(EquipmentDto Dto) : ICommand<EntityOperationResult<EquipmentDto>>;
}
