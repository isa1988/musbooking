﻿using Core.Contract.Order;
using Core.Filters;
using Core.Filters.Equipment.Check;
using FluentValidation;
using Manager.Dto;

namespace Manager.Service.Equipment.Update
{
    public class UpdateEquipmentCommandValidator : AbstractValidator<UpdateEquipmentCommand>
    {
        private readonly IEquipmentRepository equipmentRepository;

        public UpdateEquipmentCommandValidator(IEquipmentRepository equipmentRepository)
        {
            this.equipmentRepository = equipmentRepository;
            RuleFor(x => x.Dto.Name)
                .NotEmpty()
                .WithMessage("Наименование не заполнено")
                .WithState(x => ResultCode.FieldIsNull);

            RuleFor(x => x.Dto.Price)
                .Must(price => price > 0)
                .WithMessage("Цена не может быть меньше или равна нулю")
                .WithState(x => ResultCode.FieldIsNull);


            RuleFor(x => x.Dto.Amount)
                .Must(price => price > 0)
                .WithMessage("Остаток не может быть меньше или равна нулю")
                .WithState(x => ResultCode.FieldIsNull);

            RuleFor(x => x)
               .MustAsync(BeUniqueNameAndPrefix)
               .WithMessage("Наименование уже есть БД")
               .WithState(x => ResultCode.EqualInDb);

        }

        private async Task<bool> BeUniqueNameAndPrefix(UpdateEquipmentCommand dto, CancellationToken cancellationToken)
        {
            var filter = new QueryOptionsForDeleteOne<CheckNameOfEquipmentFilter>
            {
                Filter = new CheckNameOfEquipmentFilter
                {
                    Id = dto.Dto.Id,
                    Name = dto.Dto.Name,
                }
            };
            var isCheck = await equipmentRepository.IsEqualsNameAsync(filter);
            return !isCheck;
        }
    }
}
