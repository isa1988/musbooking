﻿using BuildingBlocks.CQRS;
using Manager.Dto;

namespace Manager.Service.Equipment.Create
{
    public record CreateEquipmentCommand(EquipmentDto Dto) : ICommand<EntityOperationResult<EquipmentDto>>;
}
