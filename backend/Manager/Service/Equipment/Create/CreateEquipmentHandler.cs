﻿using AutoMapper;
using BuildingBlocks.CQRS;
using Core.Contract.Order;
using FluentValidation;
using Manager.Dto;
using Manager.Helper;
using Primitive.MyException;

namespace Manager.Service.Equipment.Create
{
    public class CreateEquipmentHandler : ICommandHandler<CreateEquipmentCommand, EntityOperationResult<EquipmentDto>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IValidator<CreateEquipmentCommand> validator;
        private readonly IMapper mapper;

        public CreateEquipmentHandler(IEquipmentRepository equipmentRepository,
            IValidator<CreateEquipmentCommand> validator,
            IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.validator = validator;
            this.mapper = mapper;
        }

        public async Task<EntityOperationResult<EquipmentDto>> Handle(CreateEquipmentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var validationResult = await validator.ValidateAsync(command, cancellationToken);
                var error = validationResult.GetError();
                if (error.IsError)
                {
                    return EntityOperationResult<EquipmentDto>.Failure(error.ErrorMessage, error.Code);
                }
                var project = mapper.Map<Core.Entity.Equipment>(command.Dto);
                project = await equipmentRepository.AddAsync(project);

                await equipmentRepository.SaveAsync(cancellationToken);

                var dto = mapper.Map<EquipmentDto>(project);

                return EntityOperationResult<EquipmentDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
