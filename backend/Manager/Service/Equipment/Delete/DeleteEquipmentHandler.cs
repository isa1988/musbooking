﻿using AutoMapper;
using BuildingBlocks.CQRS;
using Core.Contract.Order;
using Core.Filters;
using Manager.Dto;
using Primitive.MyException;

namespace Manager.Service.Equipment.Delete
{
    public class DeleteEquipmentHandler : ICommandHandler<DeleteEquipmentCommand, EntityOperationResult<EquipmentDto>>
    {
        private readonly IEquipmentRepository equipmentRepository;
        private readonly IMapper mapper;

        public DeleteEquipmentHandler(IEquipmentRepository equipmentRepository,
            IMapper mapper)
        {
            this.equipmentRepository = equipmentRepository;
            this.mapper = mapper;
        }

        public async Task<EntityOperationResult<EquipmentDto>> Handle(DeleteEquipmentCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var filter = new QueryOptionsOne<Guid>
                {
                    Filter = command.Id,
                };
                var project = await equipmentRepository.GetByIdAsync(filter);

                equipmentRepository.Delete(project);

                await equipmentRepository.SaveAsync(cancellationToken);

                var dto = mapper.Map<EquipmentDto>(project);

                return EntityOperationResult<EquipmentDto>.Success(dto);
            }
            catch (RawDbNullException ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.LineIsNotSearch);
            }
            catch (Exception ex)
            {
                return EntityOperationResult<EquipmentDto>.Failure(ex.Message, ResultCode.General);
            }
        }
    }
}
