﻿using BuildingBlocks.CQRS;
using Manager.Dto;

namespace Manager.Service.Equipment.Delete
{
    public record DeleteEquipmentCommand(Guid Id) : ICommand<EntityOperationResult<EquipmentDto>>;
}
