﻿using BuildingBlocks.Behaviors;
using FluentValidation;
using Manager.Service.Equipment.Create;
using Manager.Service.Equipment.Update;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FeatureManagement;
using System.Reflection;

namespace Manager
{
    public static class DependencyInjectionCQRC
    {
        public static IServiceCollection AddApplicationServices
        (this IServiceCollection services)
        {
            services.AddMediatR(config =>
            {
                config.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
                //config.AddOpenBehavior(typeof(ValidationBehavior<,>));
                config.AddOpenBehavior(typeof(LoggingBehavior<,>));
            });

            services.AddFeatureManagement();

            services.AddScoped<IValidator<CreateEquipmentCommand>, CreateEquipmentCommandValidator>();
            services.AddScoped<IValidator<UpdateEquipmentCommand>, UpdateEquipmentCommandValidator>();

            return services;
        }


    }
}
