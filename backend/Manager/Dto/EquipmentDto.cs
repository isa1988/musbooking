﻿using Manager.Helper;
using Primitive;

namespace Manager.Dto
{
    public class EquipmentDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public Deleted IsDeleted { get; set; }

        public void ClearSpace()
        {
            Name = Name.Trim();
        }
    }
}
