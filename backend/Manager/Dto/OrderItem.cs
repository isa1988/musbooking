﻿using Manager.Helper;

namespace Manager.Dto
{
    public class OrderItem : IDto
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid OrderId { get; set; }
        public OrderDto? Order { get; set; }
        public Guid EquipmentId { get; set; }
        public EquipmentDto? Equipment { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public void ClearSpace()
        {
            Order.ClearSpacesOfDto();
            Equipment.ClearSpacesOfDto();
        }
    }
}
