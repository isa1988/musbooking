﻿namespace Manager.Dto
{
    public interface IDtoGeneral
    {
        void ClearSpace();
    }
    public interface IDto : IDtoGeneral
    {
        // Сделал б так 
        //DateTime CreateAt { get; set; }
        //DateTime ModifyAt { get; set; }
    }

    public interface IDto<TId> : IDto
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
