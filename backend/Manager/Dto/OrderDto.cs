﻿using Manager.Helper;

namespace Manager.Dto
{
    public class OrderDto : IDto<Guid>
    {
        public Guid Id { get; set; }
        public string Description { get; set; } = string.Empty;
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public decimal Price { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();

        public void ClearSpace()
        {
            Description = Description.Trim();

            OrderItemList.ClearSpacesInListOfDto();
        }
    }
}
