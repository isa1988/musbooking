﻿namespace Manager.Filters.Equipment
{
    public record EquipmentFilterDto
    {
        public string? Name { get; set; }
        public PeriodFilterDto<decimal>? Price { get; set; }
        public PeriodFilterDto<int>? Amount { get; set; }
    }
}
