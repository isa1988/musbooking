﻿namespace Manager.Filters
{
    public record PeriodFilterDto<T>
    where T : struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
