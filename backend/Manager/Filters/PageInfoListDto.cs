﻿using Manager.Dto;

namespace Manager.Filters
{
    public record PageInfoListDto<T, TOrder> : IDtoGeneral
        where TOrder : Enum
    {
        public int TotalCount { get; set; }
        public List<T> ValueList { get; set; } = new List<T>();
        public PagingOrderSettingDto<TOrder>? OrderSetting { get; set; }

        public void ClearSpace()
        {

        }
    }
}
