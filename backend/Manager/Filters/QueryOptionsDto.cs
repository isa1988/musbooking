﻿namespace Manager.Filters
{
    public record QueryOptionsDto<T, TOrder>
        where TOrder : Enum
    {
        public T Filter { get; set; }
        public PagingOrderSettingDto<TOrder>? PagingOrderSetting { get; set; }
    }
}
