﻿using Core.DataBaseInitializer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace DAL.Data
{
    public class DataBaseInitializer : IDataBaseInitializer
    {
        private readonly IServiceProvider serviceProvider;

        public DataBaseInitializer(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }
        public async Task InitializeAsync()
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<MUSbookingContext>();
                await context.Database.MigrateAsync();
            }
        }
    }
}
