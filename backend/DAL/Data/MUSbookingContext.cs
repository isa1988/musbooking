﻿using Core.Entity;
using DAL.Data.Configuration;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    public class MUSbookingContext : DbContext
    {
        public MUSbookingContext(DbContextOptions<MUSbookingContext> options)
            : base(options) 
        {            
        }

        public virtual DbSet<Equipment> Equipment { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderItem> OrderItem { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EquipmentConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderItemConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
