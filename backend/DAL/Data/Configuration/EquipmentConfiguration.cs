﻿using Core.Entity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration
{
    internal class EquipmentConfiguration : IEntityTypeConfiguration<Equipment>
    {
        public void Configure(EntityTypeBuilder<Equipment> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");

            builder.Property(e => e.Name).IsRequired().HasMaxLength(4000);
            
            //это на случай примера уникальности через настройки поля в таблицы
            //builder.HasIndex(e => e.Name).IsUnique();
            builder.Property(e => e.Price).IsRequired().HasColumnType("decimal(12, 4)");
        }
    }
}
