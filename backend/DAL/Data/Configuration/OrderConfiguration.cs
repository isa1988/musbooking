﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Core.Entity;

namespace DAL.Data.Configuration
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("uuid_generate_v4()");

            builder.Property(e => e.CreatedAt)
                .HasColumnType("timestamp without time zone")
                .HasDefaultValueSql("NOW() at time zone \'utc\'")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.Description).IsRequired().HasMaxLength(4000);

            builder.Property(e => e.Price).IsRequired().HasColumnType("decimal(18, 4)");
        }
    }
}
