﻿using Microsoft.EntityFrameworkCore.Migrations;
using System.Reflection;
using System.Text;

namespace DAL.Helper
{
    static class MigrationSQLHelperNameFolder
    {
        public const string SQL = "SQL";
        public const string UP = "UP";
        public const string DOWN = "DOWN";
        public const string CREATE = "CREATE";
        public const string UPDATE = "UPDATE";
        public const string DELETE = "DELETE";
    }

    internal class MigrationSQLHelper
    {
        /// <summary>
        /// Executes SQL from the filepath
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="filename">Relative filepath</param>
        public static void ProceedEveryFileInTheFolder(MigrationBuilder migrationBuilder, string filename)
        {
            string location = Assembly.GetExecutingAssembly().Location;
            UriBuilder uri = new(location);
            string path = Uri.UnescapeDataString(uri.Path);
            var file = Path.GetDirectoryName(path) + filename;
            using var reader = new StreamReader(file);
            var content = reader.ReadToEnd();
            if (!string.IsNullOrEmpty(content))
                migrationBuilder.Sql(content);
        }

        /// <summary>
        /// Executes SQL from path. Path contains splitted parts and is assembled depending on the runtime
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="filenamearray">Splitted path parts</param>
        public static void ProceedEveryFileInTheFolder(MigrationBuilder migrationBuilder, string[] filenamearray)
        {
            //Console.WriteLine($"Path separator is: [{Path.DirectorySeparatorChar}] System is: [{Path.DirectorySeparatorChar}]");
            StringBuilder sb = new();
            foreach (string filename in filenamearray)
            {

                sb.Append(Path.DirectorySeparatorChar);
                sb.Append(filename);
            }

            string location = Assembly.GetExecutingAssembly().Location;
            //Console.WriteLine($"Path is [{location}]");
            var file = Path.GetDirectoryName(location) + sb.ToString();
            using var reader = new StreamReader(file);
            var content = reader.ReadToEnd();
            if (!string.IsNullOrEmpty(content))
                migrationBuilder.Sql(content);
        }
    }
}
