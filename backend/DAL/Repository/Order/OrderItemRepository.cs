﻿using Core.Contract.Order;
using Core.Entity;
using Core.Filters;
using Core.Filters.OrderItem;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Sort;

namespace DAL.Repository.Order
{
    public class OrderItemRepository : Repository<OrderItem, OrderItemFields>, IOrderItemRepository
    {
        public OrderItemRepository(MUSbookingContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<OrderItem>> GetAllAsync(QueryOptions<OrderItemByOrderFilter, OrderItemFields> filter)
        {
            var query = GetOrderItemByOrderFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }
        public async Task<int> GetAllCountAsync(QueryOptionsCount<OrderItemByOrderFilter> filter)
        {
            var query = GetOrderItemByOrderFilterQuery(filter.Filter, filter);
            var count = await query.CountAsync();

            return count;
        }

        private IQueryable<OrderItem> GetOrderItemByOrderFilterQuery(OrderItemByOrderFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.OrderId == filter.OrderId);

            return query;
        }

        public async Task<List<OrderItem>> GetAllAsync(QueryOptions<OrderItemByIdListFilter, OrderItemFields> filter)
        {
            var query = GetOrderItemByIdListFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }
        public async Task<int> GetAllCountAsync(QueryOptionsCount<OrderItemByIdListFilter> filter)
        {
            var query = GetOrderItemByIdListFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }
        private IQueryable<OrderItem> GetOrderItemByIdListFilterQuery(OrderItemByIdListFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.OrderId == filter.OrderId && filter.EquipmentIdIdList.Any(eId => x.EquipmentId == eId));

            return query;
        }
        public async Task<OrderItem?> GetByIdAsync(QueryOptionsForDeleteOne<OrderItemOneFilter> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.EquipmentId == filter.Filter.EquipmentId && x.OrderId == filter.Filter.OrderId);
            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }

        protected override void ClearDbSetForInclude(OrderItem entity)
        {
            
        }

        protected override IQueryable<OrderItem> OrderSort(IQueryable<OrderItem> query, IOrderSetting<OrderItemFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null ||
               !pagingOrderSetting.PagingOrderSetting.IsOrder ||
               pagingOrderSetting.PagingOrderSetting.OrderField == OrderItemFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderItemFields.CreateAt:
                        {
                            query = query.OrderBy(x => x.CreatedAt);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderItemFields.CreateAt:
                        {
                            query = query.OrderByDescending(x => x.CreatedAt);
                            break;
                        }
                }
            }

            return query;
        }
        protected override IQueryable<OrderItem> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            throw new NotImplementedException();
        }

    }
}
