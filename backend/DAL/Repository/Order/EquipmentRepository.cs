﻿using Core.Contract.Order;
using Core.Entity;
using Core.Filters;
using Core.Filters.Equipment;
using Core.Filters.Equipment.Check;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Sort;

namespace DAL.Repository.Order
{
    public class EquipmentRepository : RepositoryForDeletedGuid<Equipment, EquipmentFields>, IEquipmentRepository
    {
        public EquipmentRepository(MUSbookingContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<Equipment>> GetAllAsync(QueryOptionsForDelete<EquipmentFilter, EquipmentFields> filter)
        {
            var query = GetEquipmentFilterQuery(filter.Filter, filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<EquipmentFilter> filter)
        {
            var query = GetEquipmentFilterQuery(filter.Filter, filter, filter);
            var count = await query.CountAsync();

            return count;
        }

        private IQueryable<Equipment> GetEquipmentFilterQuery(EquipmentFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter,
            IQuerySettingSelectForDelete querySettingSelectForDelete)
        {
            var query = ResolveInclude(behaviorFilter);

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                query = query.Where(x => x.Name.Contains(filter.Name));
            }
            
            if (filter.Price != null)
            {
                if (filter.Price.Start.HasValue)
                    query = query.Where(x => x.Price >= filter.Price.Start);
                if (filter.Price.End.HasValue)
                    query = query.Where(x => filter.Price.End.HasValue && x.Price < filter.Price.End);
            }

            if (filter.Amount != null)
            {
                if (filter.Amount.Start.HasValue)
                    query = query.Where(x => filter.Amount.Start.HasValue && x.Amount >= filter.Amount.Start);
                if (filter.Amount.End.HasValue)
                    query = query.Where(x => filter.Amount.End.HasValue && x.Amount < filter.Amount.End);
            }

            query = SetQueryForDelete(query, querySettingSelectForDelete);
            return query;
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfEquipmentFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            if (filter.Filter.Id.HasValue)
                query = query.Where(x => x.Id != filter.Filter.Id);
            query = SetQueryForDelete(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(Equipment entity)
        {
            
        }

        protected override IQueryable<Equipment> OrderSort(IQueryable<Equipment> query, IOrderSetting<EquipmentFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null ||
               !pagingOrderSetting.PagingOrderSetting.IsOrder ||
               pagingOrderSetting.PagingOrderSetting.OrderField == EquipmentFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case EquipmentFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case EquipmentFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<Equipment> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<Equipment> query = GetQueryTrackingBehavior(resolveOptions);
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
