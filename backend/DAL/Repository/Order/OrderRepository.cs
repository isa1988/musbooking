﻿using Core.Contract.Order;
using Core.Filters;
using Core.Filters.Order;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Sort;

namespace DAL.Repository.Order
{
    public class OrderRepository : RepositoryGuid<Core.Entity.Order, OrderFields>, IOrderRepository
    {
        public OrderRepository(MUSbookingContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<Core.Entity.Order>> GetAllAsync(QueryOptions<OrderFilter, OrderFields> filter)
        {
            var query = GetEquipmentFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<OrderFilter> filter)
        {
            var query = GetEquipmentFilterQuery(filter.Filter, filter);
            var count = await query.CountAsync();

            return count;
        }

        private IQueryable<Core.Entity.Order> GetEquipmentFilterQuery(OrderFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            if (!string.IsNullOrWhiteSpace(filter.Description))
            {
                query = query.Where(x => x.Description.Contains(filter.Description));
            }

            if (filter.Price != null)
            {
                if (filter.Price.Start.HasValue)
                    query = query.Where(x => x.Price >= filter.Price.Start);
                if (filter.Price.End.HasValue)
                    query = query.Where(x => filter.Price.End.HasValue && x.Price < filter.Price.End);
            }

            if (filter.CreatedAt != null)
            {
                if (filter.CreatedAt.Start.HasValue)
                    query = query.Where(x => filter.CreatedAt.Start.HasValue && x.CreatedAt >= filter.CreatedAt.Start);
                if (filter.CreatedAt.End.HasValue)
                    query = query.Where(x => filter.CreatedAt.End.HasValue && x.CreatedAt < filter.CreatedAt.End);
            }


            if (filter.UpdatedAt != null)
            {
                if (filter.UpdatedAt.Start.HasValue)
                    query = query.Where(x => filter.UpdatedAt.Start.HasValue && x.UpdatedAt >= filter.UpdatedAt.Start);
                if (filter.UpdatedAt.End.HasValue)
                    query = query.Where(x => filter.UpdatedAt.End.HasValue && x.UpdatedAt < filter.UpdatedAt.End);
            }

            return query;
        }

        protected override void ClearDbSetForInclude(Core.Entity.Order entity)
        {

        }

        protected override IQueryable<Core.Entity.Order> OrderSort(IQueryable<Core.Entity.Order> query, IOrderSetting<OrderFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null ||
               !pagingOrderSetting.PagingOrderSetting.IsOrder ||
               pagingOrderSetting.PagingOrderSetting.OrderField == OrderFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderFields.CreateAt:
                        {
                            query = query.OrderBy(x => x.CreatedAt);
                            break;
                        }
                    case OrderFields.Price:
                        {
                            query = query.OrderBy(x => x.Price);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderFields.CreateAt:
                        {
                            query = query.OrderByDescending(x => x.CreatedAt);
                            break;
                        }
                    case OrderFields.Price:
                        {
                            query = query.OrderByDescending(x => x.Price);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<Core.Entity.Order> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            throw new NotImplementedException();
        }
    }
}
