﻿namespace Primitive.Helper
{
    public static class EnumHelper
    {
        public static Deleted GetUnStatus(this Deleted value)
        {
            var retVal = value == Deleted.UnDeleted ? Deleted.Deleted : Deleted.UnDeleted;
            return retVal;
        }
    }
}
