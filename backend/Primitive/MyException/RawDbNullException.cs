﻿namespace Primitive.MyException
{
    public class RawDbNullException : NullReferenceException
    {
        public RawDbNullException(string error) : base(error)
        {

        }
    }
}
