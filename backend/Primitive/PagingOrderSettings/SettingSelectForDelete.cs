﻿namespace Primitive.PagingOrderSettings
{
    public enum SettingSelectForDelete : int
    {
        All = 0,
        OnlyDelete,
        OnlyUnDelete,
    }
}
