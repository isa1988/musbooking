﻿namespace Primitive.PagingOrderSettings.Sort
{
    public enum OrderType
    {
        ASC,
        DESC
    }
}
