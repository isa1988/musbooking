﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Sort
{
    public enum EquipmentFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По наименованию")]
        Name,

        [Display(Name = "По цене")]
        Price,

        [Display(Name = "По остатку")]
        Amount
    }
}
