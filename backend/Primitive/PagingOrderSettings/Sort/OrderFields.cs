﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Sort
{
    public enum OrderFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По дате созздания")]
        CreateAt,

        [Display(Name = "По сумме")]
        Price,
    }
}
