﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Sort
{
    public enum OrderItemFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По дате созздания")]
        CreateAt,
    }
}
