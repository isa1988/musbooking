﻿using System.ComponentModel;

namespace Primitive
{
    public enum Deleted
    {
        [Description("Не помечанный на удаление")]
        UnDeleted = 0,

        [Description("Помечанный на удаление")]
        Deleted = 1,
    }
}
