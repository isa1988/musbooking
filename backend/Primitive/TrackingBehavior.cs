﻿namespace Primitive
{
    public enum TrackingBehavior
    {
        AsTracking,
        AsNoTracking
    }
}
