﻿namespace Core.Entity
{
    public class Order : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string Description { get; set; } = string.Empty;
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public decimal Price { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();
    }
}
