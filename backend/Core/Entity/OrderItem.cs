﻿namespace Core.Entity
{
    public class OrderItem : IEntity
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public Guid OrderId { get; set; }
        public Order? Order { get; set; }
        public Guid EquipmentId { get; set; }
        public Equipment? Equipment { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
    }
}
