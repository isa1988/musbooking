﻿namespace Core.Entity
{
    public interface IEntity
    {
        // Сделал б так 
        //DateTime CreateAt { get; set; }
        //DateTime ModifyAt { get; set; }
    }

    public interface IEntity<TId> : IEntity
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
