﻿using Primitive;

namespace Core.Entity
{
    public class Equipment : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();
    }
}
