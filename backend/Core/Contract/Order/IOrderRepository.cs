﻿using Core.Filters;
using Primitive.PagingOrderSettings.Sort;
using Core.Filters.Order;

namespace Core.Contract.Order
{
    public interface IOrderRepository : IRepository<Core.Entity.Order, Guid, OrderFields>
    {
        Task<List<Core.Entity.Order>> GetAllAsync(QueryOptions<OrderFilter, OrderFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<OrderFilter> filter);
    }
}
