﻿using Core.Entity;
using Core.Filters;
using Core.Filters.OrderItem;
using Primitive.PagingOrderSettings.Sort;

namespace Core.Contract.Order
{
    public interface IOrderItemRepository : IRepository<OrderItem, OrderItemFields>
    {
        Task<List<OrderItem>> GetAllAsync(QueryOptions<OrderItemByOrderFilter, OrderItemFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<OrderItemByOrderFilter> filter);

        Task<List<OrderItem>> GetAllAsync(QueryOptions<OrderItemByIdListFilter, OrderItemFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<OrderItemByIdListFilter> filter);
        Task<OrderItem?> GetByIdAsync(QueryOptionsForDeleteOne<OrderItemOneFilter> filter);
    }
}
