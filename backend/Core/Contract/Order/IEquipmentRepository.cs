﻿using Core.Entity;
using Core.Filters;
using Core.Filters.Equipment;
using Core.Filters.Equipment.Check;
using Primitive.PagingOrderSettings.Sort;

namespace Core.Contract.Order
{
    public interface IEquipmentRepository : IRepositoryForDeleted<Equipment, Guid, EquipmentFields>
    {
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfEquipmentFilter> filter);
        Task<List<Equipment>> GetAllAsync(QueryOptions<EquipmentFilter, EquipmentFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<EquipmentFilter> filter);
    }
}
