﻿namespace Core.Helper
{
    public record PagingPageSetting
    {
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
    }
}
