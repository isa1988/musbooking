﻿namespace Core.Helper
{
    public record ResolveOptions
    {
        public bool IsEquipment { get; set; }

        public bool IsOrderItemList { get; set; }
    }
}
