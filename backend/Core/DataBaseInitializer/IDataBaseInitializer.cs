﻿namespace Core.DataBaseInitializer
{
    public interface IDataBaseInitializer
    {
        Task InitializeAsync();
    }
}
