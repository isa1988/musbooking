﻿using Core.Helper;
using Primitive;

namespace Core.Filters
{
    public record QueryOfTrackingBehaviorFilter
    {
        public TrackingBehavior TrackingBehavior { get; set; } = TrackingBehavior.AsTracking;
        public ResolveOptions? ResolveOptions { get; set; }
    }
}
