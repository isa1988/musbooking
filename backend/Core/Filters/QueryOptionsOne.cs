﻿namespace Core.Filters
{
    public record QueryOptionsOne<T> : QueryOfTrackingBehaviorFilter
    {
        public T Filter { get; set; }
    }
}
