﻿namespace Core.Filters.Order
{
    public record OrderFilter
    {
        public PeriodFilter<DateTime>? CreatedAt { get; set; }
        public PeriodFilter<DateTime>? UpdatedAt { get; set; }
        public PeriodFilter<decimal>? Price { get; set; }
        public string? Description { get; set; }
    }
}
