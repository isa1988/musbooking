﻿
namespace Core.Filters
{
    public record PeriodFilter<T>
    where T : struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
