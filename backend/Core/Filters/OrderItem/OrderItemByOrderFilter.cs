﻿namespace Core.Filters.OrderItem
{
    public record OrderItemByOrderFilter
    {
        public Guid OrderId { get; set; }
    }
}
