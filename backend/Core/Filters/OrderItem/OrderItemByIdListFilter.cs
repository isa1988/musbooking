﻿namespace Core.Filters.OrderItem
{
    public record OrderItemByIdListFilter
    {
        public Guid OrderId { get; set; }
        public List<Guid> EquipmentIdIdList { get; set; } = new List<Guid>();
    }
}
