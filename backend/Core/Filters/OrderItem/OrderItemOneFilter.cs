﻿namespace Core.Filters.OrderItem
{
    public record OrderItemOneFilter
    {
        public Guid OrderId { get; set; }
        public Guid EquipmentId { get; set; }
    }
}
