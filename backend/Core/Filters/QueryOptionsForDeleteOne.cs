﻿using Primitive.PagingOrderSettings;

namespace Core.Filters
{
    public record QueryOptionsForDeleteOne<T> : QueryOptionsOne<T>, IQuerySettingSelectForDelete
    {
        public SettingSelectForDelete SettingSelectForDelete { get; set; }
    }
}
