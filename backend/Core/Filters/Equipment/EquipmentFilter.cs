﻿namespace Core.Filters.Equipment
{
    public record EquipmentFilter
    {
        public string? Name { get; set; }
        public PeriodFilter<decimal>? Price { get; set; }
        public PeriodFilter<int>? Amount { get; set; }
    }
}
