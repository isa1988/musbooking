﻿namespace Core.Filters.Equipment.Check
{
    public record CheckNameOfEquipmentFilter : CheckFilterById
    {
        public string Name { get; set; } = string.Empty;
    }
}
