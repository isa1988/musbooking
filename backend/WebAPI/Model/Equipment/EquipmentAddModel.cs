﻿namespace WebAPI.Model.Equipment
{
    public class EquipmentAddModel
    {
        public string Name { get; set; } = string.Empty;
        public int Amount { get; set; }
        public decimal Price { get; set; }
    }
}
