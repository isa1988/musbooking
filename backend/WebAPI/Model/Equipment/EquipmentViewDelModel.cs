﻿using Primitive;

namespace WebAPI.Model.Equipment
{
    public class EquipmentViewDelModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public Deleted IsDeleted { get; set; }
    }
}
