﻿namespace WebAPI.Model.Filter
{
    public class EnumInfo
    {
        public string Title { get; set; } = string.Empty;
        public int Code { get; set; }
    }
}
