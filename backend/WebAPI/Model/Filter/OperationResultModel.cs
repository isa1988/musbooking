﻿using Manager.Service;

namespace WebAPI.Model.Filter
{
    public class OperationResultModel<T>
    {
        public string? Message { get; set; } = string.Empty;
        public ResultCode Code { get; set; }
        public T? Result { get; set; }
        public bool IsSuccess
        {
            get { return Code == ResultCode.Success; }
        }
    }
}
