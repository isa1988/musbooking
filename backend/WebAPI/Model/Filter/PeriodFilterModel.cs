﻿namespace WebAPI.Model.Filter
{
    public record PeriodFilterModel<T>
    where T : struct
    {
        public T? Start { get; set; }
        public T? End { get; set; }
    }
}
