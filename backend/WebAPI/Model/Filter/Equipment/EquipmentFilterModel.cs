﻿namespace WebAPI.Model.Filter.Equipment
{
    public class EquipmentFilterModel
    {
        public string? Name { get; set; }
        public PeriodFilterModel<decimal>? Price { get; set; }
        public PeriodFilterModel<int>? Amount { get; set; }
    }
}
