﻿using Manager.Filters;

namespace WebAPI.Model.Filter
{
    public class QueryOptionsModel<T, TOrder>
        where TOrder : Enum
    {
        public T Filter { get; set; }
        public PagingOrderSettingDto<TOrder>? PagingOrderSetting { get; set; }
    }
}
