﻿namespace WebAPI.Model.Filter
{
    public class PageInfoListModel<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        public int TotalCount { get; set; }
        public List<T> ValueList { get; set; } = new List<T>();
        public PagingOrderSettingModel<TOrder> OrderSetting { get; set; } = new PagingOrderSettingModel<TOrder>();
    }
}
