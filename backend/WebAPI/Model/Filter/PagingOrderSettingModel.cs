﻿using Primitive.PagingOrderSettings.Sort;

namespace WebAPI.Model.Filter
{
    public class PagingOrderSettingModel<T>
        where T : Enum
    {
        public int StartPosition { get; set; }
        public int PageSize { get; set; }
        public bool IsOrder { get; set; }
        public T OrderField { get; set; }
        public OrderType OrderDirection { get; set; }
    }
}
