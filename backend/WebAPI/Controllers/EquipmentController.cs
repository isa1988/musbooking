﻿using AutoMapper;
using Manager.Dto;
using Manager.Filters;
using Manager.Filters.Equipment;
using Manager.Service;
using Manager.Service.Equipment.Create;
using Manager.Service.Equipment.Delete;
using Manager.Service.Equipment.Queries.GetAll;
using Manager.Service.Equipment.Queries.GetAllWithDelete;
using Manager.Service.Equipment.Queries.GetById;
using Manager.Service.Equipment.Update;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Primitive.PagingOrderSettings.Sort;
using WebAPI.Helper;
using WebAPI.Model.Equipment;
using WebAPI.Model.Filter;
using WebAPI.Model.Filter.Equipment;

namespace WebAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EquipmentController : ControllerBase
    {
        private readonly ISender sender;
        private readonly IMapper mapper;

        public EquipmentController(ISender sender,
            IMapper mapper)
        {
            this.sender = sender;
            this.mapper = mapper;
        }


        /// <summary>
        /// Поля для сортировки оборудования
        /// </summary>
        /// <returns></returns>
        [HttpGet("infoOrderEquipment")]
        public async Task<List<EnumInfo>> GetInfoOrderEquipmentAsync()
        {
            var model = HelperMapping.GetInfoOfEnumAsync<EquipmentFields>(); 
            return model;
        }

        /// <summary>
        /// Список оборудования
        /// </summary>
        [HttpPost("all")]
        public async Task<OperationResultModel<PageInfoListModel<EquipmentViewModel, EquipmentFields>>> 
            GetAllAsync(QueryOptionsModel<EquipmentFilterModel, EquipmentFields> filter)
        {
            var filterDto = mapper.Map<QueryOptionsDto<EquipmentFilterDto, EquipmentFields>>(filter);
            var result = await sender.Send(new GetAllEquipmentQuery(filterDto));
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<EquipmentViewModel, EquipmentFields>>(result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<PageInfoListModel<EquipmentViewModel, EquipmentFields>>
                    (null, result.Code, result.ErrorMessage);
            }
        }

        /// <summary>
        /// Список оборудования с удаленными
        /// </summary>
        [HttpPost("allWithDeleted")]
        public async Task<OperationResultModel<PageInfoListModel<EquipmentViewDelModel, EquipmentFields>>>
            GetAllWithDeletedAsync(QueryOptionsModel<EquipmentFilterModel, EquipmentFields> filter)
        {
            var filterDto = mapper.Map<QueryOptionsDto<EquipmentFilterDto, EquipmentFields>>(filter);
            var result = await sender.Send(new GetAllWithDeleteEquipmentQuery(filterDto));
            if (result.IsSuccess)
            {
                var model = mapper.Map<PageInfoListModel<EquipmentViewDelModel, EquipmentFields>>(result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<PageInfoListModel<EquipmentViewDelModel, EquipmentFields>>
                    (null, result.Code, result.ErrorMessage);
            }
        }

        // <summary>
        /// Вытащить одно оборудование
        /// </summary>
        [HttpGet("id/{id:guid}")]
        public async Task<OperationResultModel<EquipmentViewModel>> GetByIdAsync(Guid id)
        {
            var result = await sender.Send(new GetByIdEquipmentQuery(id));
            if (result.IsSuccess)
            {
                var model = mapper.Map<EquipmentViewModel>(result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<EquipmentViewModel>(null, result.Code, result.ErrorMessage);
            }
        }


        /// <summary>
        /// Добавить оборудование
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<OperationResultModel<EquipmentViewModel>> CreateAsync(EquipmentAddModel request)
        {
            var filterDto = mapper.Map<EquipmentDto>(request);
            
            var result = await sender.Send(new CreateEquipmentCommand(filterDto));
            if (result.IsSuccess)
            {
                var model = mapper.Map<EquipmentViewModel> (result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<EquipmentViewModel>
                    (null, result.Code, result.ErrorMessage);
            }
        }

        /// <summary>
        /// Редактировать оборудование
        /// </summary>
        /// <returns></returns>
        [HttpPut("edit")]
        public async Task<OperationResultModel<EquipmentViewModel>> EditAsync(EquipmentEditModel request)
        {
            var filterDto = mapper.Map<EquipmentDto>(request);

            var result = await sender.Send(new UpdateEquipmentCommand(filterDto));
            if (result.IsSuccess)
            {
                var model = mapper.Map<EquipmentViewModel>(result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<EquipmentViewModel>
                    (null, result.Code, result.ErrorMessage);
            }
        }

        /// <summary>
        /// Удалить заказ
        /// </summary>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<OperationResultModel<EquipmentViewModel>> DeleteAsync(Guid id)
        {
            var result = await sender.Send(new DeleteEquipmentCommand(id));
            if (result.IsSuccess)
            {
                var model = mapper.Map<EquipmentViewModel>(result.Entity);
                return HelperMapping.GetOperationResult(model, ResultCode.Success, string.Empty);
            }
            else
            {
                return HelperMapping.GetOperationResult<EquipmentViewModel>
                    (null, result.Code, result.ErrorMessage);
            }
        }
    }
}
