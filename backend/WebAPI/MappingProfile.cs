﻿using AutoMapper;
using Manager.Dto;
using Manager.Filters;
using Manager.Filters.Equipment;
using WebAPI.Model.Equipment;
using WebAPI.Model.Filter;
using WebAPI.Model.Filter.Equipment;

namespace WebAPI
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            EquipmentMapping();
            GeneralMapping();
        }

        public void EquipmentMapping()
        {
            CreateMap<EquipmentAddModel, EquipmentDto>()
                .ForMember(x => x.Id, p => p.Ignore())
                .ForMember(x => x.IsDeleted, p => p.Ignore());
            CreateMap<EquipmentEditModel, EquipmentDto>()
                .ForMember(x => x.IsDeleted, p => p.Ignore());

            CreateMap<EquipmentDto, EquipmentViewDelModel>();
            CreateMap<EquipmentDto, EquipmentViewModel>();

            CreateMap<EquipmentFilterModel, EquipmentFilterDto>();
        }

        private void GeneralMapping()
        {
            CreateMap(typeof(PagingOrderSettingDto<>), typeof(PagingOrderSettingModel<>));

            CreateMap(typeof(PeriodFilterModel<>), typeof(PeriodFilterDto<>));

            CreateMap(typeof(QueryOptionsModel<,>), typeof(QueryOptionsDto<,>));
            CreateMap(typeof(PageInfoListDto<,>), typeof(PageInfoListModel<,>));

        }
    }
}
