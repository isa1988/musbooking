﻿using Manager.Service;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using WebAPI.Model.Filter;

namespace WebAPI.Helper
{
    public static class HelperMapping
    {
        public static OperationResultModel<T> GetOperationResult<T>(T? model, ResultCode code, string? error)
        {
            return new OperationResultModel<T>
            {
                Result = model,
                Code = code,
                Message = error,
            };
        }

        public static List<EnumInfo> GetInfoOfEnumAsync<TEnum>()
           where TEnum : Enum
        {
            var values = Enum.GetValues(typeof(TEnum));
            var retList = new List<EnumInfo>();
            foreach (var value in values)
            {
                retList.Add(new EnumInfo
                {
                    Title = ((TEnum)value).GetEnumDisplayName(),
                    Code = (int)value,
                });
            }

            return retList;
        }

        public static string GetEnumDisplayName(this Enum enumType)
        {
            var memberInfo = enumType.GetType().GetMember(enumType.ToString()).First();

            var displayAttribute = memberInfo.GetCustomAttribute<DisplayAttribute>();

            return displayAttribute?.Name ?? string.Empty;
        }
    }
}
