﻿using DAL.Data;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Helper
{
    public class MUSbookingContextFactory : IDesignTimeDbContextFactory<MUSbookingContext>
    {
        public MUSbookingContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Development.json")
                .Build();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<MUSbookingContext>();
            optionsBuilder.UseNpgsql(connectionString);

            return new MUSbookingContext(optionsBuilder.Options);
        }
    }
}
